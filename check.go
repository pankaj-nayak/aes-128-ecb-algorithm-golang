package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strings"
)

var (
	conn *tls.Conn
	err  error
)

func main() {
	/*
	 * src string to be encrypted
	 * key The key used for encryption. The key length can be any of 128bit, 192bit, and 256bit.
	 * 16-bit key corresponds to 128bit
	 */
	//src := "CID=2835&RID=123456&CRN=123456&AMT=1&VER=1.0&TYP=TEST&CNY=INR&RTU=http://localhost:9092/testing/&PPI=1|1|1|1|1|1&RE1=MN&RE2=&RE3=&RE4=&RE5=&CKS=ad5a4845d25e41a94ff9d520a59556063f756536511e8b718186d7c7599a0778"
	key := "axisbank12345678"

	src := "CID=5773&RID=789452&CRN=789454&AMT=10500.00&VER=1.0&TYP=Test&CNY=INR&RTU=https://www.nemsssrajhara.org/&PPI=Nusery|4900|1400.00|10500.00&RE1=MN&RE2=&RE3=&RE4=&RE5=&CKS=d858f0ab70f8be422136c0f9528b79066cc390fd311b217784b8ce0e7b3a0b05"
	crypted := AesEncrypt(src, key)
	AesDecrypt(crypted, []byte(key))
	Base64URLDecode("39W7dWTd_SBOCM8UbnG6qA")
}

func Base64URLDecode(data string) ([]byte, error) {
	var missing = (4 - len(data)%4) % 4
	data += strings.Repeat("=", missing)
	res, err := base64.URLEncoding.DecodeString(data)
	fmt.Println("decodebase64urlsafe is:", string(res), err)
	return base64.URLEncoding.DecodeString(data)
}

func Base64UrlSafeEncode(source []byte) string {
	// Base64 Url Safe is the same as Base64 but does not contain ‘/‘ and ‘+‘ (replaced by ‘_’ and ‘-’) and trailing ‘=‘ are removed.
	bytearr := base64.StdEncoding.EncodeToString(source)
	safeurl := strings.Replace(string(bytearr), "/", "_", -1)
	safeurl = strings.Replace(safeurl, "+", "-", -1)
	safeurl = strings.Replace(safeurl, "=", "", -1)
	return safeurl
}

func AesDecrypt(crypted, key []byte) []byte {
	block, err := aes.NewCipher(key)
	if err != nil {
		fmt.Println("err is:", err)
	}
	blockMode := NewECBDecrypter(block)
	origData := make([]byte, len(crypted))
	blockMode.CryptBlocks(origData, crypted)
	origData = PKCS5UnPadding(origData)
	fmt.Println("source is:", origData, string(origData))
	return origData
}

func AesEncrypt(src, key string) []byte {
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		fmt.Println("key error1", err)
	}
	if src == "" {
		fmt.Println("plain content empty")
	}
	ecb := NewECBEncrypter(block)
	content := []byte(src)
	content = PKCS5Padding(content, block.BlockSize())
	crypted := make([]byte, len(content))
	ecb.CryptBlocks(crypted, content)
	// ordinary base64 encoding encryption is different from urlsafe base64
	fmt.Println("base64 result:", base64.StdEncoding.EncodeToString(crypted))
	Post(base64.StdEncoding.EncodeToString(crypted))
	fmt.Println("base64UrlSafe result:", Base64UrlSafeEncode(crypted))
	return crypted
}

func Post(dst string) {

	tlsConfig := http.DefaultTransport.(*http.Transport).TLSClientConfig

	apiURL := "https://uat-etendering.axisbank.co.in/easypay2.0/frontend/index.php/api/payment"
	// resource := "/user/"
	data := url.Values{}
	data.Set("i", dst)
	// data.Set("surname", "bar")

	u, _ := url.ParseRequestURI(apiURL)
	u.RawQuery = data.Encode()

	//u.Path = resource
	//u.Host = "https://www.nemsssrajhara.org/"
	urlStr := fmt.Sprintf("%v", u) // "https://api.com/user/"

	client := &http.Client{
		Transport: &http.Transport{
			DialTLS: func(network, addr string) (net.Conn, error) {
				conn, err = tls.Dial(network, addr, tlsConfig)
				return conn, err
			},
		},
	}
	fmt.Println("Parameters: ", data.Encode())
	r, _ := http.NewRequest(http.MethodPost, urlStr, nil)
	//r.Header.Add("Host", "https://www.nemsssrajhara.org/")
	//r.URL.Host = "https://www.nemsssrajhara.org/"
	//r.Host = "https://www.nemsssrajhara.org/"
	res, _ := client.Do(r)
	fmt.Println(res.Status)
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("response body :%s ", body)

	// req, _ := http.NewRequest("POST", "https://uat-etendering.axisbank.co.in/easypay2.0/frontend/index.php/api/payment", nil)
	// req.URL.Query().Add("i", dst)
	// // I wish explicitly setting this Host header would take precedence
	// req.Header.Set("Host", "www.example.org")
	// //req.Write(os.Stdout)

	// //res, err := c.Get("https://uat-etendering.axisbank.co.in/easypay2.0/frontend/index.php")
	// res, err := c.PostForm("https://uat-etendering.axisbank.co.in/easypay2.0/frontend/index.php/api/payment", url.Values{"i": []string{dst}})
	// if err != nil {
	// 	//		log.Fatal(err)
	// 	fmt.Println("Error : ", err)
	// }
	// defer res.Body.Close()
	// body, err := ioutil.ReadAll(res.Body)

	// fmt.Printf("response body :%s ", body)
	// versions := map[uint16]string{
	// 	tls.VersionSSL30: "SSL",
	// 	tls.VersionTLS10: "TLS 1.0",
	// 	tls.VersionTLS11: "TLS 1.1",
	// 	tls.VersionTLS12: "TLS 1.2",
	// 	tls.VersionTLS13: "TLS 1.3",
	// }

	// fmt.Println(res.Request.URL)
	// fmt.Println(res.Request.Host)
	// fmt.Println(res.Status)
	// v := conn.ConnectionState().Version
	// fmt.Println("version :", v)
	// fmt.Println(versions[v])
}
func PKCS5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func PKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	// remove the last byte unpadding times
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

type ecb struct {
	b         cipher.Block
	blockSize int
}

func newECB(b cipher.Block) *ecb {
	return &ecb{
		b:         b,
		blockSize: b.BlockSize(),
	}
}

type ecbEncrypter ecb

// NewECBEncrypter returns a BlockMode which encrypts in electronic code book
// mode, using the given Block.
func NewECBEncrypter(b cipher.Block) cipher.BlockMode {
	return (*ecbEncrypter)(newECB(b))
}
func (x *ecbEncrypter) BlockSize() int { return x.blockSize }
func (x *ecbEncrypter) CryptBlocks(dst, src []byte) {
	if len(src)%x.blockSize != 0 {
		panic("crypto / cipher: input not full blocks")
	}
	if len(dst) < len(src) {
		panic("crypto / cipher: output smaller than input")
	}
	for len(src) > 0 {
		x.b.Encrypt(dst, src[:x.blockSize])
		src = src[x.blockSize:]
		dst = dst[x.blockSize:]
	}
}

type ecbDecrypter ecb

// NewECBDecrypter returns a BlockMode which decrypts in electronic code book
// mode, using the given Block.
func NewECBDecrypter(b cipher.Block) cipher.BlockMode {
	return (*ecbDecrypter)(newECB(b))
}
func (x *ecbDecrypter) BlockSize() int { return x.blockSize }
func (x *ecbDecrypter) CryptBlocks(dst, src []byte) {
	if len(src)%x.blockSize != 0 {
		panic("crypto / cipher: input not full blocks")
	}
	if len(dst) < len(src) {
		panic("crypto / cipher: output smaller than input")
	}
	for len(src) > 0 {
		x.b.Decrypt(dst, src[:x.blockSize])
		src = src[x.blockSize:]
		dst = dst[x.blockSize:]
	}
}
