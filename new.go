package main

import (
	"crypto/sha256"
	"fmt"

	"net"
	"net/http"
	"net/url"
	//	"crypto/aes"
	"encoding/base64"
	"golang.org/x/crypto/blowfish"

	"crypto/tls"
	"github.com/andreburgaud/crypt2go/ecb"
	"github.com/andreburgaud/crypt2go/padding"
)

var (
	conn *tls.Conn
	err  error
)

func encrypt(pt, key []byte) []byte {
	block, err := blowfish.NewCipher(key)
	//	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}
	mode := ecb.NewECBEncrypter(block)
	padder := padding.NewPkcs5Padding()
	pt, err = padder.Pad(pt) // pad last block of plaintext if block size less than block cipher size
	if err != nil {
		panic(err.Error())
	}
	ct := make([]byte, len(pt))
	mode.CryptBlocks(ct, pt)
	return ct
}

func decrypt(ct, key []byte) []byte {
	block, err := blowfish.NewCipher(key)
	if err != nil {
		panic(err.Error())
	}
	mode := ecb.NewECBDecrypter(block)
	pt := make([]byte, len(ct))
	mode.CryptBlocks(pt, ct)
	padder := padding.NewPkcs5Padding()
	pt, err = padder.Unpad(pt) // unpad plaintext after decryption
	if err != nil {
		panic(err.Error())
	}
	return pt
}

func example() {

	encryptionKey := "axisbank12345678"
	checksumKey := "axis"

	// cid := "3636"
	// rid := "698431"
	// crn := "698431"
	// amt := "50.00"
	// ver := "1.0"
	// typ := "Test"
	// cny := "INR"
	// rtu := "https://www.test.com/"
	// ppi := "frank lin|01/10/2018|9787780523|frank@frank.com|50.00"
	// re1 := "MN"

	cid := "5773"
	rid := "789452"
	crn := "789454"
	amt := "10500.00"
	ver := "1.0"
	//typ := "Test"
	typ := "Test"
	cny := "INR"
	rtu := "https://www.nemsssrajhara.org/"
	//rtu := "http://localhost:9092/testing/"
	//ppi := "1|1|1|1|1|1"
	ppi := "Nusery|4900|1400.00|10500.00"
	re1 := "MN"
	// re2 := ""
	// re3 := ""
	// re4 := ""
	// re5 := ""

	//2Xz4q2QUhPOEgItN3d8mR16blJal1lUYu/4tzwl+bBFzN2xu8ayZq520nw+4/+votfi64WoFsYrgkSJTwSH Yb5LLxOfyHlH53GzSLyPE7J3Ih0HnGsAalYCwLGpXRh5usTXplN0gsfnIdWZ9MbUW+lEBRpomm1FG 78cq48CwWD5qGn6H5W7wMvMOdkpJgD5cN5gF6eNlx0gzTdZDo2N0axQhAGy0GubNeEmsHTZJvDC 8uNOcxpnU1MvaY/Pr/fBciE7pLXIwHN79wvYyRthKYfY2STbgG6Qcr+xpll7RefU=

	// Values: AES128(CID=2835&RID=123456&CRN=123456&AMT=1&VER=1.0&TYP=TEST&CNY=INR&RTU=http://l
	// 	ocalhost:9092/testing/&PPI=1|1|1|1|1|1&RE1=MN&RE2=&RE3=&RE4=&RE5=&CKS=ad5a4845d25e41a94ff9
	// 	d520a59556063f756536511e8b718186d7c7599a0778)
	//	hash := sha256.Sum256(cid + rid + crn + amt + checksumKey)
	h := sha256.New()
	h.Write([]byte(cid))
	h.Write([]byte(rid))
	h.Write([]byte(crn))
	h.Write([]byte(amt))
	h.Write([]byte(checksumKey))
	cks := h.Sum(nil)
	fmt.Printf("checksumHash :%x \n", cks)

	s := fmt.Sprintf("%x", cks)
	fmt.Println("s", s)

	request := "CID=" + cid + "&RID=" + rid + "&CRN=" + crn + "&AMT=" + amt + "&VER=" + ver + "&TYP=" + typ + "&CNY=" + cny + "&RTU=" + rtu + "&PPI=" + ppi + "&RE1=" + re1 + "&RE2=&RE3=&RE4=&RE5=&CKS=" + s
	//string(cks)

	pt := []byte(request)
	key := []byte(encryptionKey)

	ct := encrypt(pt, key)
	fmt.Println()
	fmt.Printf("Ciphertext: %x\n", base64.StdEncoding.EncodeToString(ct))
	fmt.Println()

	dst := fmt.Sprintf("%x", ct)
	//fmt.Println("s", dst)

	tlsConfig := http.DefaultTransport.(*http.Transport).TLSClientConfig

	c := &http.Client{
		Transport: &http.Transport{
			DialTLS: func(network, addr string) (net.Conn, error) {
				conn, err = tls.Dial(network, addr, tlsConfig)
				return conn, err
			},
		},
	}
	res, err := c.PostForm("https://uat-etendering.axisbank.co.in/easypay2.0/frontend/index.php/api/payment", url.Values{"i": {(dst)}})
	if err != nil {
		//log.Fatal(err)
	}

	versions := map[uint16]string{
		tls.VersionSSL30: "SSL",
		tls.VersionTLS10: "TLS 1.0",
		tls.VersionTLS11: "TLS 1.1",
		tls.VersionTLS12: "TLS 1.2",
		tls.VersionTLS13: "TLS 1.3",
	}

	fmt.Println(res.Request.URL)
	fmt.Println(res.Status)
	v := conn.ConnectionState().Version
	fmt.Println("version :", v)
	fmt.Println(versions[v])

	recovered_pt := decrypt(ct, key)
	fmt.Println()
	fmt.Printf("Recovered plaintext: %s\n", recovered_pt)
	fmt.Println()
}

func main() {
	example()
}
